import tweepy
from pyexcel_ods import get_data

def retweet_bot():
	try:
		userlist = []
		data = get_data("input.ods")
		for d in data:
			out = {'name':d[0],'api_key':d[1],'api_secret':d[2],'access_token':d[3],'access_secret':d[4]}
			userlist.append(out)
		for user in userlist:
			auth = tweepy.OAuthHandler(user['api_key'], user['api_secret'])
			auth.set_access_token(user['access_token'], user['access_secret'])
			api = tweepy.API(auth)
			for status in api.user_timeline('tncpim'):
				res = api.retweet(status.id)
				print user['name']+" "+"Retweeted Successfully"
				break
	except Exception as e:
		print e

if __name__ == "__main__":
	retweet_bot() 
	

